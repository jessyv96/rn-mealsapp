import React, { useState, useEffect, useCallback } from 'react'
import { StyleSheet, View, Switch, Platform } from 'react-native'
import { useDispatch } from 'react-redux'
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../components/CustomHeaderButton'
import DefaultText from '../components/DefaultText';
import TitleText from '../components/TitleText';
import Colors from '../constants/Colors';
import { setFilters } from '../store/actions/meals.actions'

const FiltersSwitch = ({ label, state, onSetValue }) => {
    return <View style={styles.switchContainer}>
        <DefaultText style={styles.text}>{label}</DefaultText>
        <Switch
            trackColor={{ true: Colors.primaryLight }}
            thumbColor={Platform.OS === 'android' ? Colors.primaryLight : ''}
            value={state}
            onValueChange={onSetValue} />
    </View>
}

const FiltersScreen = ({ navigation }) => {
    const dispatch = useDispatch()
    const [isGlutenFree, setIsGlutenFree] = useState(false)
    const [isLactoseFree, setIsLactoseFree] = useState(false)
    const [isVegatarian, setIsVegatarian] = useState(false)
    const [isVegan, setIsVegan] = useState(false)

    const filtersSave = useCallback(() => {
        const appliedFilter = {
            isGlutenFree,
            isLactoseFree,
            isVegatarian,
            isVegan,
        }
        dispatch(setFilters(appliedFilter))
        navigation.navigate('Categories')
    }, [isGlutenFree, isLactoseFree, isVegatarian, isVegan])

    useEffect(() => {
        navigation.setParams({ filters: filtersSave })
    }, [filtersSave])

    return (
        <View style={styles.screen}>
            <TitleText style={styles.title}>Filtes disponibles</TitleText>
            <FiltersSwitch
                label='Sans Gluten'
                state={isGlutenFree}
                onSetValue={newValue => setIsGlutenFree(newValue)} />
            <FiltersSwitch
                label='Sans Lactose'
                state={isLactoseFree}
                onSetValue={newValue => setIsLactoseFree(newValue)} />
            <FiltersSwitch
                label='Vegan'
                state={isVegan}
                onSetValue={newValue => setIsVegan(newValue)} />
            <FiltersSwitch
                label='Végetarien'
                state={isVegatarian}
                onSetValue={newValue => setIsVegatarian(newValue)} />
        </View>
    )
}

FiltersScreen.navigationOptions = navData => {
    return {
        title: 'Repas filtré', // un title est ajouté par default via createStackNavigator
        headerLeft: () => (
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item
                    title='mainMenu'
                    iconName='ios-menu'
                    onPress={() => navData.navigation.toggleDrawer()} />
            </HeaderButtons>
        ),
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item
                    title='saveFilter'
                    iconName='ios-save'
                    onPress={navData.navigation.getParam('filters')} />
            </HeaderButtons>
        ),
    }
}

export default FiltersScreen

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        marginVertical: 15
    },
    text: {

    },
    switchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 20,
        width: '80%'
    }
})
