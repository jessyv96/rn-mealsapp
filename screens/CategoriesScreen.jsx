import React from 'react'
import { StyleSheet, FlatList } from 'react-native'
import { CATEGORIES } from '../data/dummy-data'
import CategoryGridTile from '../components/CategoryGridTile';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../components/CustomHeaderButton'


const CategoriesScreen = ({ navigation }) => {
    const renderItemGrid = (itemData) => {
        return <CategoryGridTile
            onSelect={() => {
                navigation.navigate('CategoryMeals', {
                    categoryId: itemData.item.id
                })
            }}
            title={itemData.item.title}
            color={itemData.item.color}
        />
    }

    return (
        <FlatList
            data={CATEGORIES}
            numColumns={2}
            renderItem={renderItemGrid} />
    )
}

CategoriesScreen.navigationOptions = navData => {
    return {
        title: 'Catégories de repas', // un title est ajouté par default via createStackNavigator
        headerLeft: () => 
        <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='mainMenu'
                iconName='ios-menu'
                onPress={() => navData.navigation.toggleDrawer()} />
        </HeaderButtons>
    }
}

export default CategoriesScreen

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
