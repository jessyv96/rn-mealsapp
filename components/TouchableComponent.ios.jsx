import React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'

const TouchableComponent = ({ children, style, onSelect }) => {
    return (
        <TouchableOpacity onPress={onSelect} style={{ ...style }}>
            {children}
        </TouchableOpacity>
    )
}

export default TouchableComponent