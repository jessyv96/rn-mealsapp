import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';
import { enableScreens } from 'react-native-screens';
import * as Font from 'expo-font';

import MealsNavigator from './navigation/MealNavigator'

import mealsReducer from './store/reducers/meals.reducer'

enableScreens();

const mainReducer = combineReducers({
  meals: mealsReducer
})
const store = createStore(mainReducer, applyMiddleware(thunk), composeWithDevTools())

export default function App() {
  const [loaded] = Font.useFonts({
    openSans: require('./assets/fonts/OpenSans-Regular.ttf'),
    openSansBold: require('./assets/fonts/OpenSans-Bold.ttf'),
  });

  if (!loaded) {
    return null;
  }

  return (
    <Provider store={store}>
      <MealsNavigator />
    </Provider>
  )
}