import React from 'react'
import { StyleSheet, View, FlatList } from 'react-native'

import MealItem from './MealItem';


const MealsList = ({ listItem, navigation }) => {
    const renderMealItem = itemData => {
        return <MealItem
            onSelectMeal={() => navigation.navigate(
                'MealDetails',
                { mealId: itemData.item.id, mealtitle: itemData.item.title }
            )}
            title={itemData.item.title}
            image={itemData.item.imageUrl}
            complexity={itemData.item.complexity}
            affordability={itemData.item.affordability}
            duration={itemData.item.duration} />
    }

    return (
        <View style={styles.list}>
            <FlatList
                data={listItem}
                renderItem={renderMealItem}
                style={{ width: '100%' }} />
        </View>
    )
}

export default MealsList

const styles = StyleSheet.create({
    list: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 18,
        marginVertical: 13
    }
})
