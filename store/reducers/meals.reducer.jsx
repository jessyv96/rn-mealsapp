import { MEALS } from '../../data/dummy-data'
import { RESET_FILTER, SET_FILTERS, TOGGLE_FAVORITE } from '../actions/meals.actions'

const initialState = {
    meals: MEALS,
    filteredMeals: MEALS,
    favoriteMeals: []
}

const mealsReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case TOGGLE_FAVORITE:
            const existingMeal = state.favoriteMeals.findIndex(meal => meal.id === actions.payload)
            if (existingMeal >= 0) {
                const updateFavMeals = [...state.favoriteMeals]
                updateFavMeals.splice(existingMeal, 1)
                return { ...state, favoriteMeals: updateFavMeals }
            } else {
                const meal = state.meals.find(meal => meal.id === actions.payload)
                return { ...state, favoriteMeals: state.favoriteMeals.concat(meal) }
            }
        case SET_FILTERS:
            return { ...state, filteredMeals: actions.payload } //Pourquoi on retourne le state
        default:
            return state
    }

}

export default mealsReducer