import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import TouchableComponent from './components/TouchableComponent';

const CategoryGridTile = ({ onSelect, title, color }) => {
    return (
        <View
            style={styles.categoryItem}>
            <TouchableComponent
                style={{ flex: 1 }}
                onSelect={onSelect}>
                <View style={{ ...styles.container, ...{ backgroundColor: color } }}>
                    <Text style={styles.categoryText}>{title}</Text>
                </View>
            </TouchableComponent>
        </View>
    )
}

export default CategoryGridTile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.6,
        shadowRadius: 10,
        shadowOffset: { width: 0, height: 2 },
        elevation: 3
    },
    categoryItem: {
        flex: 1,
        margin: 20,
        height: 130,
    },
    categoryText: {
        fontFamily: 'openSans',
        fontSize: 16
    }
})
