import React from 'react'
import { TouchableNativeFeedback, TouchableOpacity, Platform, StyleSheet } from 'react-native'

const TouchableComponent = ({ children, style, onSelect }) => {
    return (
        Platform.Version >= 21
            ? (
                <TouchableNativeFeedback onPress={onSelect} style={{ ...style }}>
                    {children}
                </TouchableNativeFeedback>
            )
            : (
                <TouchableOpacity onPress={onSelect} style={{ ...style }}>
                    {children}
                </TouchableOpacity>
            )
    )
}

export default TouchableComponent