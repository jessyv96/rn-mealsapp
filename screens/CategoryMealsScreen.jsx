import React from 'react'
import { useSelector } from 'react-redux'
import { CATEGORIES } from '../data/dummy-data'
import MealsList from '../components/MealsList';
import { View, StyleSheet } from 'react-native';
import DefaultText from '../components/DefaultText';

const CategoryMealsScreen = ({ navigation }) => {
    const cid = navigation.getParam('categoryId')
    const mealslist = useSelector(state => state.meals)
    const { filteredMeals } = mealslist

    const mealsByCategory = filteredMeals.filter(meal => meal.categoriesId.includes(cid))

    if (mealsByCategory.length === 0) {
        return <View style={styles.screen}>
            <DefaultText>Pas de plat trouvé, Vérifiez vos filtres</DefaultText>
        </View>
    }

    return <MealsList
        listItem={mealsByCategory}
        navigation={navigation} />
}

CategoryMealsScreen.navigationOptions = navigationData => {
    const cid = navigationData.navigation.getParam('categoryId')
    const category = CATEGORIES.find(cat => cat.id === cid)
    return {
        headerTitle: category.title,
    }

}

export default CategoryMealsScreen

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
