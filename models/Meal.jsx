class Meal {
    constructor(
        id,
        categoriesId,
        title,
        affordability,
        complexity,
        imageUrl,
        duration,
        ingedients,
        steps, 
        isGlutenFree, 
        isVegan, 
        isVegeterian, 
        isLactoseFree) {
        this.id = id
        this.categoriesId = categoriesId
        this.title = title
        this.affordability = affordability
        this.complexity = complexity
        this.imageUrl = imageUrl
        this.duration = duration
        this.ingedients = ingedients
        this.steps = steps
        this.isGlutenFree = isGlutenFree
        this.isVegan = isVegan
        this.isVegeterian = isVegeterian
        this.isLactoseFree = isLactoseFree
    }
}

export default Meal;