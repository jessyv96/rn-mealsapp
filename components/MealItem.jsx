import React from 'react'
import { StyleSheet, Text, View, ImageBackground } from 'react-native'
import TouchableComponent from './TouchableComponent'
import Colors from '../constants/Colors';
import DefaultText from './DefaultText'

const MealItem = ({
    title,
    duration,
    complexity,
    affordability,
    image,
    onSelectMeal }) => {
    return (
        <View style={styles.mealItem}>
            <TouchableComponent onSelect={onSelectMeal}>
                <View>
                    <View style={{ ...styles.row, ...styles.mealHeader }}>
                        <ImageBackground style={styles.bgImage} source={{ uri: image }}>
                            <View style={styles.titleContainer}>
                                <Text style={styles.title} numberOfLines={1}>{title}</Text>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={{ ...styles.row, ...styles.mealDetails }}>
                        <DefaultText>{duration} min</DefaultText>
                        <DefaultText>{complexity.toUpperCase()}</DefaultText>
                        <DefaultText>{affordability.toUpperCase()}</DefaultText>
                    </View>
                </View>
            </TouchableComponent>
        </View>
    )
}

export default MealItem

const styles = StyleSheet.create({
    mealItem: {
        height: 200,
        marginVertical: 8,
        width: '100%',
        backgroundColor: Colors.primaryLight,
        borderRadius: 10,
        overflow: 'hidden'
    },
    bgImage: {
        width: '100%',
        height: '100%',
        justifyContent: 'flex-end',

    },
    row: {
        flexDirection: 'row'
    },
    mealHeader: {
        height: '85%',

    },
    mealDetails: {
        height: '15%',
        paddingHorizontal: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    titleContainer: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        paddingVertical: 5,
        paddingHorizontal: 12
    },
    title: {
        fontFamily: 'openSansBold',
        fontSize: 22,
        color: 'white',
        textAlign: 'center'
    },
})
