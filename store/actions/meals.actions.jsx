export const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE'
export const SET_FILTERS = 'SET_FILTERS'

export const toogleFavorite = mealId => (dispatch) => {
    dispatch({
        type: TOGGLE_FAVORITE,
        payload: mealId
    })
}

export const setFilters = filterSettings => (dispatch, getState) => {
    const { meals: { meals } } = getState() //pourquoi ça fonctionne avec meals et pas filteredMeals
    const { isGlutenFree, isLactoseFree, isVegatarian, isVegan } = filterSettings

    const updateMealFilter = meals.filter(meal => {
        if (isGlutenFree && !meal.isGlutenFree) {
            return false
        } else if (isLactoseFree && !meal.isLactoseFree) {
            return false
        } else if (isVegatarian && !meal.isVegatarian) {
            return false
        } else if (isVegan && !meal.isVegan) {
            return false
        } else {
            return true
        }
    })

    dispatch({
        type: SET_FILTERS,
        payload: updateMealFilter
    })
}
