import React, { useCallback, useEffect } from 'react'
import { StyleSheet, Text, View, ScrollView, Image, Dimensions } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import CustomHeaderButton from '../components/CustomHeaderButton'
import DefaultText from '../components/DefaultText'

import { toogleFavorite } from '../store/actions/meals.actions'

const ListItem = ({ children }) => {
    return <View style={styles.listItem}>
        <DefaultText>{children}</DefaultText>
    </View>
}

const MealDetailsScreen = ({ navigation }) => {
    const dispatch = useDispatch();
    const mealId = navigation.getParam('mealId')
    const mealslist = useSelector(state => state.meals)
    const { meals, favoriteMeals } = mealslist
    const getMeal = meals.find(meal => meal.id === mealId)
    const isFavMeal = favoriteMeals.some(meal => meal.id === mealId)


    const toggleFavoriteHandler = useCallback(() => {
        dispatch(toogleFavorite(mealId))
    }, [dispatch, mealId])

    useEffect(() => {
        navigation.setParams({ isFavMeal: isFavMeal })
        navigation.setParams({ 'toggleFav': toggleFavoriteHandler })
    }, [toggleFavoriteHandler, isFavMeal])

    return (
        <ScrollView>
            <Image
                source={{ uri: getMeal.imageUrl }}
                style={styles.image} />
            <View style={styles.details}>
                <DefaultText>{getMeal.duration} min</DefaultText>
                <DefaultText>{getMeal.complexity.toUpperCase()}</DefaultText>
                <DefaultText>{getMeal.affordability.toUpperCase()}</DefaultText>
            </View>
            <Text style={styles.title}>ingredients</Text>
            {getMeal.ingedients.map(ingredient =>
                <ListItem key={ingredient}>{ingredient}</ListItem>
            )}
            <Text style={styles.title}>étape</Text>
            {getMeal.steps.map(step =>
                <ListItem key={step}>{step}</ListItem>
            )}
        </ScrollView>
    )
}

MealDetailsScreen.navigationOptions = navigationData => {
    const mealTitle = navigationData.navigation.getParam('mealTitle')
    const toggleFav = navigationData.navigation.getParam('toggleFav')
    const isFavMeal = navigationData.navigation.getParam('isFavMeal')
    return {
        headerTitle: mealTitle,
        headerRight: () =>
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item
                    title='favorite'
                    iconName={isFavMeal ? 'ios-star' : 'ios-star-outline'}
                    onPress={toggleFav} />
            </HeaderButtons>
    }
}

export default MealDetailsScreen

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        width: '100%',
        height: Dimensions.get('window').height / 3
    },
    details: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 15
    },
    title: {
        fontFamily: 'openSansBold',
        textTransform: 'capitalize',
        marginVertical: 10,
        fontSize: 20,
        textAlign: 'center'
    },
    listItem: {
        marginVertical: 10,
        marginHorizontal: 15,
        padding: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 10
    }
})
