import React from 'react'
import { Platform, Text } from 'react-native'
import { Ionicons } from '@expo/vector-icons'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { createDrawerNavigator } from 'react-navigation-drawer';

import CategoriesScreen from '../screens/CategoriesScreen'
import CategoryMealsScreen from '../screens/CategoryMealsScreen'
import MealDetailsScreen from '../screens/MealDetailsScreen'
import FavoriteScreen from '../screens/FavoriteScreen'
import FiltersScreen from '../screens/FiltersScreen'

import Colors from '../constants/Colors';

const defaultStackOptions = {
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
    },
    headerTitleStyle: {
        fontSize: Platform.OS === 'android' ? 16 : 18,
        fontFamily: 'openSansBold'
    },
    headerBackTitleStyle: {
        fontFamily: 'openSans'
    },
    headerTintColor: Platform.OS === 'android' ? Colors.text : Colors.primary
}

const MealsNavigator = createStackNavigator({
    Categories: CategoriesScreen,
    CategoryMeals: CategoryMealsScreen,
    MealDetails: MealDetailsScreen
}, {
    defaultNavigationOptions: defaultStackOptions
})

const FavoriteNavigator = createStackNavigator({
    Favorites: FavoriteScreen,
    MealDetails: MealDetailsScreen
}, {
    defaultNavigationOptions: defaultStackOptions
})

const FilterNavigator = createStackNavigator({
    Filter: FiltersScreen,
}, {
    defaultNavigationOptions: defaultStackOptions
})

const tabScreenConfig = {
    Meals: {
        screen: MealsNavigator, navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Ionicons name='ios-restaurant' size={25} color={tabInfo.tintColor} />
            },
            tabBarLabel: Platform.OS === 'android' ? <Text style={{ fontFamily: 'openSansBold' }}>Repas</Text> : 'Repas'

        }
    },
    Favorites: {
        screen: FavoriteNavigator, navigationOptions: {
            tabBarIcon: (tabInfo) => {
                return <Ionicons name='ios-star' size={25} color={tabInfo.tintColor} />
            },
            tabBarLabel: Platform.OS === 'android' ? <Text style={{ fontFamily: 'openSansBold' }}>Favoris</Text> : 'Favoris'
        }
    }
}

const MealsFavTabsNavigator =
    Platform.OS === 'android'
        ? createMaterialBottomTabNavigator(
            tabScreenConfig, {
            activeColor: Colors.text,
            shifting: true,
            barStyle: {
                backgroundColor: Colors.primary
            }
        })
        : createBottomTabNavigator(
            tabScreenConfig, {
            tabBarOptions: {
                labelStyle: {
                    fontFamily: 'openSansBold'
                },
                activeTintColor: Colors.primary
            }
        })

const MainNavigator = createDrawerNavigator({
    Meals: {
        screen: MealsFavTabsNavigator,
        navigationOptions: {
            drawerLabel: 'Repas'
        }
    },
    Filters: {
        screen: FilterNavigator,
        navigationOptions: {
            drawerLabel: 'Filtres'
        }
    },
}, {
    contentOptions: {
        activeTintColor: Colors.secondary,
        labelStyle: {
            fontFamily: 'openSans',
            fontWeight: 'normal'
        }
    }
})

export default createAppContainer(MainNavigator)