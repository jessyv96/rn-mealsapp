export default {
    primary: '#6e77af',
    primaryLight: '#ABA3CC',
    secondary: '#EBB3A9',
    text: 'white'
} 