import React from 'react'
import { useSelector } from 'react-redux'
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import MealsList from '../components/MealsList';
import CustomHeaderButton from '../components/CustomHeaderButton'
import { View, StyleSheet } from 'react-native';
import DefaultText from '../components/DefaultText';


const FavoriteScreen = ({ navigation }) => {
    const meals = useSelector(state => state.meals)
    const { favoriteMeals } = meals

    if (favoriteMeals.length === 0) {
        return <View style={styles.screen}>
            <DefaultText>Pas de plat en favoris, ajoutez-en !</DefaultText>
        </View>
    }

    return <MealsList
        listItem={favoriteMeals}
        navigation={navigation} />
}

FavoriteScreen.navigationOptions = navData => {
    return {
        title: 'Mes favoris',
        headerLeft: () =>
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item
                    title='mainMenu'
                    iconName='ios-menu'
                    onPress={() => { navData.navigation.toggleDrawer() }} />
            </HeaderButtons>
    }
}

export default FavoriteScreen

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
